Scrapper
===
El script puede extraer información desde un RSS/XML de un sitio de noticias, por mencionar algunos:
- https://www.cooperativa.cl/noticias/stat/rss/rss.html
- https://www.elmostrador.cl/destacado/feed/
- https://www.latercera.com/feed/

Prerequisitos
---
- Python +3.8
- Libreria "requests"
- Windows +10
- Tener permisos de escritura en la ubicacion del programa
- Contar con espacio suficiente para escribir el archivo (3 registros 8 [kb] )
- Debe contar con acceso a internet
- Tener configurado el proxy a nivel de comandos previa ejecución del programa.

Sintaxis
---
```shell
$ ./Scrapper <URL RSS>
```

Ejemplo
---
```shell
$ ./Scrapper https://www.elmostrador.cl/destacado/feed/
```

Archivos de salida
---
Los archivos de salida tienen las siguientes características
- **Codificacion:** UTF-8
- **Separador CSV:** Pipe ( **|** )
- **Datos:** Title, Description, pubDate y guid
- **Ubicacion:** Los archivos de salida saldrán al mismo directorio donde está ubicado el programa.

Errores conocidos
---
- No existe la libreria requests (WINDOWS): Procedimiento de instalación
```shell
C:\Users\fcaballeros\Desktop\Nueva carpeta\tarea4lp-179944529>C:\Users\fcaballeros\AppData\Local\Programs\Python\Python38\Scripts\pip3.8.exe install requests
Collecting requests
  Downloading https://files.pythonhosted.org/packages/51/bd/23c926cd341ea6b7dd0b2a00aba99ae0f828be89d72b2190f27c11d4b7fb/requests-2.22.0-py2.py3-none-any.whl (57kB)
     |████████████████████████████████| 61kB 787kB/s
Collecting chardet<3.1.0,>=3.0.2
  Downloading https://files.pythonhosted.org/packages/bc/a9/01ffebfb562e4274b6487b4bb1ddec7ca55ec7510b22e4c51f14098443b8/chardet-3.0.4-py2.py3-none-any.whl (133kB)
     |████████████████████████████████| 143kB 2.2MB/s
Collecting idna<2.9,>=2.5
  Downloading https://files.pythonhosted.org/packages/14/2c/cd551d81dbe15200be1cf41cd03869a46fe7226e7450af7a6545bfc474c9/idna-2.8-py2.py3-none-any.whl (58kB)
     |████████████████████████████████| 61kB 3.8MB/s
Collecting urllib3!=1.25.0,!=1.25.1,<1.26,>=1.21.1
  Downloading https://files.pythonhosted.org/packages/b4/40/a9837291310ee1ccc242ceb6ebfd9eb21539649f193a7c8c86ba15b98539/urllib3-1.25.7-py2.py3-none-any.whl (125kB)
     |████████████████████████████████| 133kB 3.2MB/s
Collecting certifi>=2017.4.17
  Downloading https://files.pythonhosted.org/packages/b9/63/df50cac98ea0d5b006c55a399c3bf1db9da7b5a24de7890bc9cfd5dd9e99/certifi-2019.11.28-py2.py3-none-any.whl (156kB)
     |████████████████████████████████| 163kB 3.3MB/s
Installing collected packages: chardet, idna, urllib3, certifi, requests
  WARNING: The script chardetect.exe is installed in 'c:\users\fcaballeros\appdata\local\programs\python\python38\Scripts' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
Successfully installed certifi-2019.11.28 chardet-3.0.4 idna-2.8 requests-2.22.0 urllib3-1.25.7
```

- No existe el aplicativo PIP (WINDOWS) [https://www.liquidweb.com/kb/install-pip-windows/]. Es importante mencionar que al terminar la instalación aparece un warning que indica que el aplicativo PIP se instaló pero no quedó en el path del sistema operativo. Se puede ejecutar el aplicativo utilizando la ruta absoluta o bien modificando el PATH del sistema operativo.
```shell
C:\Users\fcaballeros\Desktop\Nueva carpeta>py get-pip.py
Collecting pip
  Downloading https://files.pythonhosted.org/packages/00/b6/9cfa56b4081ad13874b0c6f96af8ce16cfbc1cb06bedf8e9164ce5551ec1/pip-19.3.1-py2.py3-none-any.whl (1.4MB)
     |████████████████████████████████| 1.4MB 273kB/s
Collecting wheel
  Downloading https://files.pythonhosted.org/packages/00/83/b4a77d044e78ad1a45610eb88f745be2fd2c6d658f9798a15e384b7d57c9/wheel-0.33.6-py2.py3-none-any.whl
Installing collected packages: pip, wheel
  Found existing installation: pip 19.2.3
    Uninstalling pip-19.2.3:
      Successfully uninstalled pip-19.2.3
  WARNING: The scripts pip.exe, pip3.8.exe and pip3.exe are installed in 'C:\Users\fcaballeros\AppData\Local\Programs\Python\Python38\Scripts' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
  WARNING: The script wheel.exe is installed in 'C:\Users\fcaballeros\AppData\Local\Programs\Python\Python38\Scripts' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
Successfully installed pip-19.3.1 wheel-0.33.6
```

Change log
- v1.0: Versión inicial del componente