#!/usr/bin/python3
import sys,os
from urllib.parse import urlparse
import requests
import xml.etree.ElementTree as ET
import time, datetime

# Configuraciones
SEPARADOR = "|"
CANTIDAD_NOTICIAS = 3  # -1 para todas las noticias que haya disponible
PREFIJO_ARCHIVO = "Noticias"

"""
/************ Funcion: extract ***************** 
Descripcion: Extraer datos en formato RSS desde una direccion
Parametros:
url string 
Retorno: Archivo XML RSS 
************************************************/ 
"""
def extract(url):
    print("===== Extraccion de datos =====")
    print("URL a procesar: ", url)
    response = requests.get(url)
    if (response.status_code != 200):
        print ("El sitio respondió con un código de error: HTTP_STATUS: " + response.status_code)
        exit(1)
    else:
        print("Extraccion correcta")
        return response.text

"""
/******** Funcion: transform ***************** 
Descripcion: Transformar los datos de XML RSS a un objeto via serialización
Parametros:
xml string 
Retorno: Objeto serializado
************************************************/   
"""
def transform(xml):
    arrNoticias = []
    print("===== Transformacion de datos =====")
    tree = ET.ElementTree(ET.fromstring(xml))

    title = ""
    description = ""
    pubDate = ""
    guid = ""

    for node in tree.iter("item"):
        title = node.find("title").text
        description = node.find("description").text.replace('\n',"<br/>")
        pubDate = node.find("pubDate").text
        guid = node.find("guid").text
        arrNoticias.append([title, description, pubDate, guid])
        # print ([node.find("title").text, node.find("description").text, node.find("pubDate").text, node.find("guid").text])
    print("Transformacion correcta")
    return arrNoticias

"""
/******** Funcion: transform ***************** 
Descripcion: Generar archivos CSV con los datos de las N (Parametro CANTIDAD_NOTICIAS) primeras noticias
Parametros:
objRss AnyType 
Retorno: Resultado de la tarea de generacion de archivos
************************************************/ 
"""
def load(objRss):
    print("===== Carga de datos =====")
    # Composición de nombre del archivo
    fecha_hora = datetime.datetime.now().strftime('%y%m%d-%H%M')
    nombre_archivo = PREFIJO_ARCHIVO + fecha_hora + ".csv"
    print("Nombre de archivo: ", nombre_archivo)
    ruta_archivo = os.path.dirname(os.path.abspath(__file__)) + "/"
    print("Ruta del archivo:",ruta_archivo)
    
    # Escritura del archivo
    file = open (ruta_archivo + nombre_archivo,'w')
    file.write("title" + SEPARADOR + "description" + SEPARADOR + "pubDate" + SEPARADOR + "guid"'\n')
    i = 0
    for item in objRss:
        file.write(item[0] + SEPARADOR + item[1] + SEPARADOR + item[2]+ SEPARADOR + item[3] + '\n')
        i = i + 1
        if(i == CANTIDAD_NOTICIAS):
            break
    file.close()
    print("Carrga correcta")

"""
/******** Funcion: is_url ***************** 
Descripcion: Valida si la URL es correcta (Autor: https://www.pythond.com/22943/python-como-validar-una-url-en-python-malformado-o-no.html)
Parametros:
url: string 
Retorno: Retorna True si es válida, y false si es inválida
************************************************/ 
"""
def is_url(url): 
    try:
        result = urlparse(url) 
        return all([result.scheme, result.netloc])
    except ValueError:
        return False

"""
/******** MAIN ********/
"""
try:
    param = sys.argv[1]
except Exception:
    print("Debe ingresar un parametro")
    print("Sintaxis: ./Scrapper <URL RSS>")
    exit(1)

print("Param: ", param)

# Validar si lo que entra es una url
if (not is_url(param)):
    print("Debe ingresar una URL valida como parametro")
    exit(1)

# Procesamiento
try:
    xml = extract(param)
except Exception:
    print("Hubo un problema con la extraccion del contenido, compruebe su conexion a internet o su proxy")
    exit(1)

# Transformacion del contenido
try:
    noticias = transform(xml)
except Exception:
    print("Hubo un problema con el contenido, compruebe que este corresponde a un XML RSS FEED")
    exit(1)

# Carga del contenido
try:
    load(noticias)
except Exception:
    print("Hubo un problema con la escritura del archivo, compruebe que tiene espacio suficiente en el disco o los permisos de escritura de su usuario")
    exit(1)

print("******** FIN DEL PROGRAMA ********")